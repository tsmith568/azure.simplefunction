using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CosmoDB.Console.Model;
using Microsoft.Azure.Cosmos;
using System.Linq;

namespace Azure.SimpleFunction
{
    public static class GetCosmosDBCustomerRecord
    {
        [FunctionName("GetCosmosDBCustomerRecord")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string Id = req.Query["Id"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            Id = Id ?? data?.Id;

            string responseMessage = "";

            if (string.IsNullOrEmpty(Id))
            {
                responseMessage = "Invalid Id.";
            }
            else
            {
                var customer = new Customer();

                try
                {

                    using (var client = new CosmosClient("AccountEndpoint=https://mindcept-cosmo.documents.azure.com:443/;AccountKey=QQ8vUzwQtI4H3DQG6OYvuVUDaswQuNcoZBgtP9qnzjnR97UVHg0AIV5eKELIPFnWszn9Zuqr7XiuffQep6rjHg==;"))
                    {
                        var DB = client.GetDatabase("ChampionProductCosmoDB");
                        var table = client.GetContainer(DB.Id, "Customer2");

                        var sqlString = "Select * from c where c.customerId = " + Id;
                        var query = new QueryDefinition(sqlString);
                        FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

                        Microsoft.Azure.Cosmos.FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                        customer = queryResult.FirstOrDefault();

                        responseMessage = JsonConvert.SerializeObject(customer);
                    };

                }
                catch (Exception ex)
                {

                    throw;
                }

            }
            return new OkObjectResult(responseMessage);
        }
    }
}
